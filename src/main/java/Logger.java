
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
/**
 * Created by Andrey on 13.04.2019.
 */

/**
 * Singleton
 */
public class Logger {
    /**
     * Implementation of singleton
     */
    public static final Logger LOGGER = new Logger();

    private Logger(){}

    /**
     * Write data about start program, it's not a constructor
     * @param name - Name of file for logging
     * @return Class File object for logging
     */
    public File LogCreate(String name)
    {
        File file = new File(name);
        Date date = new Date();
        if(!file.exists())
        {
            try {
                if(!file.createNewFile())
                    System.out.println("Error: can't create log file");

                else
                    System.out.println("Log file is successfully created");
            } catch(IOException e)
            {
                System.out.println("Error: can't create log file");
            }
        }

        LogWrite("\nProgram started...",file);
        LogWrite("Start time: " + date.toString(),file);
        return file;
    }

    public void Logging(String data, File log_file)
    {
        LogWrite(data, log_file);
        System.out.println("Error: see logs");
    }

    /**
     * Writing data to log file
     * @param data string for logging
     * @param log_file file for logging
     */
    private void LogWrite(String data, File log_file)
    {
        FileOutputStream log_stream;
        try
        {
            log_stream = new FileOutputStream(log_file, true);
            byte buffer[] = (data + "\n").getBytes();
            log_stream.write(buffer);
            log_stream.close();
        }
        catch (IOException ex)
        {
            System.out.println("Error: writing in log is impossible");
            System.out.println("Error: " + ex.toString());
            return;
        }
    }

    /**
     * "Closing" file, just write phrase about it to log
     * @param log_file
     */
    public void LogClose(File log_file)
    {
        Date date = new Date();
        LogWrite("Program finished ", log_file);
        LogWrite("Finish time: " + date.toString(), log_file);
    }
}

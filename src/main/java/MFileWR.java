/**
 * Created by Andrey on 11.10.2018.
 */

import java.io.File;
import java.io.FileReader;

public class MFileWR {

    /**
     * Reading data from file to string
     * @param file File for reading
     * @param obj StringWrapper obj, plain string by meaning
     * @return Number of read symbols
     */
    public static int ReadToBuff (File file, StringWrapper obj)
    {
        FileReader file_in;
        try
        {
            file_in = new FileReader(file);
        }
        catch (java.io.FileNotFoundException e)
        {
            obj.str =  e.toString().concat(obj.str);
            return 0;
        }

        int c;
        try
        {
            while((c = file_in.read()) != -1)
                obj.str =obj.str.concat(Character.toString((char)c));
            file_in.close();
        }
        catch (java.io.IOException e)
        {
            obj.str =  e.toString().concat(obj.str);
            return 0;
        }

        return obj.str.length();
    }
}

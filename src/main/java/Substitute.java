
import java.io.*;
import java.util.*;
import java.lang.Math;

/**
 * Created by Andrey on 13.04.2019.
 */
public class Substitute implements Executor {

    /* list of consumers for this executor */
    private ArrayList<Executor> consumers_;

    private DataOutputStream output_;
    private DataInputStream input_;

    /* log file for all class instances */
    private static File log_file_;

    /* directive set for class */
    private enum lexWorker_ {TABLE, MODE, BLOCK_SIZE, SHIFT}

    /* possible values for directive MODE */
    private enum lexCoder {ENCODE, CODE}


    /* current mode */
    private int mode_;

    /* current block size */
    private int block_size_;
    private int tail;

    /* shift from start of block for this executor */
    private int shift_;

    /* supported types for this executor */
    private APPROPRIATE_TYPES[] supTypes_;

    /* pair <directive, value> */
    private Map<String, String> configData_;

    /* table with pairs <char a, char b> for crypto algorithm >*/
    private Map<Character, Character> table_;

    /* table with pairs provider adapter */
    private Map<Executor, Object> adapters;
    /* table with pairs provider adapter */
    private Map<Executor, APPROPRIATE_TYPES> adaptersTypes;

    /* string for working (code, encode) */
    private byte[] text_;

    //TODO: create obj for text, with text and text size (delete tail)

    public Substitute()
    {
        if(log_file_ == null)
            log_file_ = Logger.LOGGER.LogCreate("LogWorkers.txt");
        if(log_file_ == null)
        {
            System.out.println("Unexpected error");
            return;
        }

        mode_ = 2; // start initialization
        supTypes_ = new APPROPRIATE_TYPES[1];
        supTypes_[0] = APPROPRIATE_TYPES.BYTE;
        consumers_ = new ArrayList<>();
        configData_ = new HashMap<>();
        table_ = new HashMap<>();
        adapters = new HashMap<>();
        adaptersTypes = new HashMap<>();
    }

    public int setConfig(String config)
    {
        String err = Interpret(config);
        if(!err.equals(""))
        {
            return 6; /* Error parsing config */
        }
        text_ = new byte[block_size_];
        return 0;
    }

    public void setInput(DataInputStream input)
    {
        if(input != null)
            input_ = input;
    }

    public void setOutput(DataOutputStream output)
    {
        if(output != null)
            output_= output;
    }

    public int setConsumer(Executor consumer)
    {
        if(consumer == null)
            return 1;

        /* searching supported types for pair provider - consumer
        * - search common type
        * - create adapter of this type
        * - adding data in consumer maps (see setAdapter)
        * */
        APPROPRIATE_TYPES[] consumerTypes = consumer.getConsumedTypes();
        APPROPRIATE_TYPES commonType = null;
        for(int i = 0; i < supTypes_.length; i++)
        {
            int j = 0;
            while(j < consumerTypes.length && commonType == null)
            {
                if(supTypes_[i] == consumerTypes[j])
                    commonType = supTypes_[i];
                j++;
            }

        }
        /* mean that executor and his consumer have no commit types */
        if(commonType == null)
            return  2;
        /* so, this implements support only data in bytes */
        Object adapter = new ByteTransferAdapter();
        consumer.setAdapter(this, adapter, commonType);

        /* add this consumer in list of consumers for this executor */
        consumers_.add(consumer);
        return 0;
    }

    public APPROPRIATE_TYPES[] getConsumedTypes()
    {
        return supTypes_;
    }

    public void setAdapter(Executor provider,
                           Object adapter,
                           APPROPRIATE_TYPES type)
    {
        adapters.put(provider, adapter);
        adaptersTypes.put(provider, type);
    }

    public int put(Executor provider)
    {
        Object adapt = adapters.get(provider);
        APPROPRIATE_TYPES type = adaptersTypes.get(provider);

        switch (type)
        {
            case BYTE:
                adapt = (ByteTransferAdapter)adapt;
                int i;
                int j;
                for(j = 0; j < shift_; j++)
                {
                    Byte temp =((ByteTransferAdapter) adapt).getNextByte();
                    if(temp == null)
                        break;
                }

                i = 0;
                Byte temp;
                while((temp =((ByteTransferAdapter) adapt).getNextByte()) != null)
                {
                    if( i >= block_size_)
                        break;

                    text_[i] = temp;
                    i++;
                }
                tail = i;
                break;
            default:
                return 1;
        }
        return 0;
    }

    public int run()
    {
        try{
            if(input_ != null)
            {
                while((tail =
                        (input_.read(text_, 0, block_size_)))
                        != -1)
                {
                    String err = CodeText(tail);
                    if(!err.equals(""))
                    {
                        return 4; /*"Error with text coding"; */
                    }
                    Iterator<Executor> consumer = consumers_.iterator();
                    while(consumer.hasNext())
                    {
                        Executor exec = consumer.next();
                        exec.put(this);
                        exec.run();
                    }

                }
            }
            if(output_ == null)
            {
                if(tail != - 1)
                {
                    String err = CodeText(tail);
                    if(!err.equals(""))
                    {
                        return 4; /*"Error with text coding"; */
                    }
                    Iterator<Executor> consumer = consumers_.iterator();
                    while(consumer.hasNext())
                    {
                        Executor exec = consumer.next();
                        exec.put(this);
                        exec.run();
                    }
                }
            }
            else
            {
                String err = CodeText(tail);
                if(!err.equals(""))
                {
                    return 4; /*"Error with text coding"; */
                }
                output_.write(text_, 0, Math.min(tail, block_size_));
            }
        }catch (IOException e)
        {

        }

        return 0;
    }

    /**
     * Interpret args for creating class
     * @param configFileNameWorker name file with config
     *                             for this worker
     * @return Empty string if it's ok, string with error otherwise
     */
    private String Interpret(String configFileNameWorker)
    {
        FileReader configFileReader;
        Properties prop = new Properties();

        try{
            configFileReader = new FileReader(configFileNameWorker);
        }catch (FileNotFoundException err)
        {
            return err.toString();
        }
        try{
            prop.load(configFileReader);
        }catch (IOException err)
        {
            return err.toString();
        }

        for(int i = 0; i < lexWorker_.values().length; i++)
        {
            String key = lexWorker_.values()[i].toString();
            String value = prop.getProperty(key);
            if(value == null)
                return "Error: directive " + key + " not found";
            configData_.put(key, value);
        }

        String value = prop.getProperty(lexWorker_.MODE.toString());
        if(value == null)
            return "Error: no name of file with table";
        if(value.equals(lexCoder.CODE.toString()))
            mode_ = 1;
        else
            if(value.equals(lexCoder.ENCODE.toString()))
                mode_ = 0;
        if(mode_ == 2)
            return "Error: wrong value for directive MODE ";

        value = prop.getProperty(lexWorker_.TABLE.toString());
        if(value == null)
            return "Error: no name of file with table";
        File tableFile = new File(value);
        String err = TableReader(tableFile);
        if(!err.equals(""))
            return err;

        value = prop.getProperty(lexWorker_.BLOCK_SIZE.toString());
        if(value == null)
            return "Error: no name of file with table";
        block_size_ = Integer.parseInt(value);

        value = prop.getProperty(lexWorker_.SHIFT.toString());
        if(value == null)
            return "Error: no shift for worker";

        shift_ = Integer.parseInt(value);

        return "";
    }

    /**
     * Depending on the mode, values from table writing to different columns
     * Old version, from javalab1
     * @param file file with table
     * @return string with error or empty string
     */
    private String TableReader(File file)
    {
        StringWrapper obj = new StringWrapper();
        MFileWR.ReadToBuff(file, obj);

        int i = 0;
        char a;
        char b;
        String err;
        while(i < obj.str.length() - 1)
        {
            if(!Character.isLetterOrDigit(obj.str.charAt(i)))
                return "Error: incorrect symbol in table, line " + (i / 5 + 1);
            a = obj.str.charAt(i);
            if(i < obj.str.length() - 1)
                i++;
            else
                return "Error: incorrect symbol after symbol " + a;

            if(obj.str.charAt(i) != ' ')
                return "Error: incorrect symbol after symbol " + a;
            if(i < obj.str.length() - 1)
                i++;
            else
                return "Error: incorrect symbol after symbol " + a;

            if(!Character.isLetterOrDigit(obj.str.charAt(i)))
                return "Error: incorrect symbol in table, line " + (i / 5 + 1);
            b = obj.str.charAt(i);
            if(i < obj.str.length() - 1)
            {
                i++;
                if(obj.str.charAt(i)!= '\r' && obj.str.charAt(i)!= '\n')
                    return "Error: incorrect symbol after symbol " + b;
                if(i < obj.str.length() - 1)
                {
                    i++;
                    if(obj.str.charAt(i)!= '\r' && obj.str.charAt(i)!= '\n')
                        return "Error: incorrect symbol after symbol " + b;
                    if(i < obj.str.length() - 1)
                        i++;
                }
                else
                if(!(err = TableInsert(a, b)).equals(""))
                    return err;
            }
            else
            if(!(err = TableInsert(a, b)).equals(""))
            {
                return err;
            }
            else
                continue;
            if(!(err = TableInsert(a, b)).equals(""))
                return err;
        }
        return "";
    }

    /**
     * Internal method for table reader, insert values in table
     * Old version, from javalab1
     * @param a value 1
     * @param b value 2
     * @return string with error or empty string
     */
    private String TableInsert(Character a, Character b)
    {
        // Code
        if(mode_ == 1)
        {
            if(table_.containsKey(a) || table_.containsValue(b))
                return "Error: no bijection in table, symbols(" + a +", " + b +")";
            table_.put(a, b);
        }
        // Encode
        else
        {
            if(table_.containsKey(b) || table_.containsValue(a))
                return "Error: no bijection in table, symbols(" + a +", " + b +")";
            table_.put(b, a);
        }
        return "";
    }

    /**
     * code array with bytes
     * size of array = min(block_size, tail)
     * @param tail size of array for coding
     * @return string with error or empty string
     */
    private String CodeText(int tail)
    {
        int i = 0;
        while(i <  Math.min(block_size_, tail))
        {
            Object obj = table_.get((char)text_[i]);
            if(obj != null)
            {
                char c = (char)obj;
                text_[i] = (byte)c;
            }
            i++;
        }
        return "";
    }

    class ByteTransferAdapter implements InterfaceByteTransfer{
        private int pos = 0;

        //@Override
        public Byte getNextByte() {
            if(pos >= tail && tail > 0)
            {
                pos = 0;
                return null;
            }
            if(pos < tail)
            {
                Byte a = text_[pos];
                pos++;
                return a;
            }
            return null;
        }
    }

}


import java.io.*;
import java.util.*;

/**
 * Created by Andrey on 13.04.2019.
 */
public class Manager {

    private enum lexManager_ {
        workersNum, // directive for number of workers
        worker, // for searching config files
        Consumer, // for setting Consumer
        input, // for searching first worker
        output, // for searching last worker
        in, // input file name
        out, // output file name
        Type // for searching worker type
    }

    private DataInputStream input_;
    private DataOutputStream output_;

    /* number of workers */
    private int workersNum;

    /* map with pair <worker, config file name> */
    private Map<String, String> workersConfigs_;

    /* map with pair <workers, consumers list> */
    private Map<String, ArrayList<String>> workersList_;

    /* map with pair worker - type of worker */
    private Map<String, String> workersTypes_;

    /* set of executors */
    private ArrayList<Executor> workerSeq_ = new ArrayList<>();

    /**
     * Initialization of cards and filling in with data
     * from the file config
     *
     * @param args string array from command line
     */
    public void run(String[] args) {
        File log_file = Logger.LOGGER.LogCreate("LogManager.txt");
        if (log_file == null) {
            System.out.println("Unexpected error");
            return;
        }

        this.workersConfigs_ = new HashMap<>();
        this.workersList_ = new HashMap<>();
        this.workersTypes_ = new HashMap<>();

        String err = ParseInput_(args);
        if (!err.equals("")) {
            Logger.LOGGER.Logging(err, log_file);
            Logger.LOGGER.LogClose(log_file);
            return;
        }

        err = CreateWorkerList_();
        if (!"".equals(err)) {
            Logger.LOGGER.Logging(err, log_file);
            Logger.LOGGER.LogClose(log_file);
            return;
        }
        err = SetConsumers_();
        if (!"".equals(err)) {
            Logger.LOGGER.Logging(err, log_file);
            Logger.LOGGER.LogClose(log_file);
            return;
        }

        err = SetConfigs_();
        if (!"".equals(err)) {
            Logger.LOGGER.Logging(err, log_file);
            Logger.LOGGER.LogClose(log_file);
            return;
        }

        String startWorkerName = workersList_.get(
                lexManager_.input.toString())
                .get(0);
        startWorkerName = startWorkerName.replaceAll("\\D+", "");
        int num = Integer.parseInt(startWorkerName) - 1;
        int e = workerSeq_.get(num).run();
        if(e!= 0)
            Logger.LOGGER.Logging(
                    "Something wrong with worker" + e, log_file);

        Logger.LOGGER.LogClose(log_file);
    }


    /**
     * filling Maps workConf an workLists with pairs
     * workers-config and workers-consumer
     * <p>
     * initialize input_ and output_ fields for manager object
     *
     * @param args string array from command line
     * @return string with error, empty string in success case
     */
    private String ParseInput_(String[] args) {
        FileReader configFile; /* file for reading */
        Properties prop = new Properties();

        try {
            configFile = new FileReader(args[0]);
        } catch (FileNotFoundException err) {
            return err.toString();
        }
        try {
            prop.load(configFile);
        } catch (IOException err) {
            return err.toString();
        }

        String value = prop.getProperty(lexManager_.workersNum.toString());
        if (value == null)
            return "Error: no directive \'" +
                    lexManager_.workersNum.toString() + "\'";

        this.workersNum = Integer.parseInt(value);
        System.out.println("Parsed data: workersNum =  "
                + this.workersNum);

        /* searching configs and filling workersConfigs */
        /* from 1 because workers name like a "worker1"
        * we have not "worker0" */
        for (int i = 1; i <= this.workersNum; i++) {
            value = prop.getProperty(lexManager_.worker.toString() + i);
            if (value == null)
                return "Error: no config for \'" +
                        lexManager_.worker.toString() + i + "\'";
            workersConfigs_.put(lexManager_.worker.toString() + i, value);
        }

        for (int i = 1; i <= this.workersNum; i++) {
            value = prop.getProperty(lexManager_.worker.toString()
                    + i + lexManager_.Type.toString());
            if (value == null)
                return "Error: no Type for \'" +
                        lexManager_.worker.toString() + i + "\'";
            workersTypes_.put(lexManager_.worker.toString() + i, value);
        }



        value = prop.getProperty(lexManager_.input.toString());
        if (value == null)
            return "Error: no worker for \'" +
                    lexManager_.input.toString() + "\'";
        ArrayList<String> inputConsumerList = new ArrayList<>();
        inputConsumerList.add(value);
        workersList_.put(lexManager_.input.toString(),inputConsumerList);


        for (int i = 1; i <= this.workersNum; i++)
        {
            ArrayList<String> workerConsumersList = new ArrayList<>();

            /* searching all consumer for this worker */
            int j = 1;
            while((value = prop.getProperty(lexManager_.worker.toString() + i
                + lexManager_.Consumer.toString() + j)) != null)
            {
                workerConsumersList.add(value);
                System.out.println("worker" + i + "Consumer" + j
                        + " " + value);
                j++;
            }
            workersList_.put(lexManager_.worker.toString() + i, workerConsumersList);

            if (j == 1 && value == null)
                return "Error: no " + lexManager_.Consumer.toString() + i
                        + "for \'" + lexManager_.worker.toString() + j + "\'";
        }

        value = prop.getProperty(lexManager_.in.toString());
        FileInputStream tempIn;
        if (value == null)
            return "Error: no file for input";
        try {
            tempIn = new FileInputStream(value);

        } catch (FileNotFoundException e) {
            return e.toString();
        }
        input_ = new DataInputStream(tempIn);

        value = prop.getProperty(lexManager_.out.toString());
        FileOutputStream tempOut;
        if (value == null)
            return "Error: no file for output";
        try {
            tempOut = new FileOutputStream(value);

        } catch (FileNotFoundException e) {
            return e.toString();
        }
        output_ = new DataOutputStream(tempOut);

        return "";
    }

    /**
     * Create array of objects
     *
     * @return string with error, empty string in success case
     */
    private String CreateWorkerList_() {
        StringWrapper err = new StringWrapper();
        for (int i = 1; i <= workersList_.size() - 1; i++) {
            try{
                String className = workersTypes_.get
                        (lexManager_.worker.toString() + i);
                Executor worker = (Executor)
                        Class.forName(className).
                                getDeclaredConstructor().newInstance();
                this.workerSeq_.add(worker);
            }catch (ReflectiveOperationException e)
            {
                return e.toString();
            }

        }
        return err.str;
    }

    /**
     * Set consumers for all objects in sequence
     * Also set input and output for 1st and last objects
     */
    private String SetConsumers_() {
        ArrayList tmpList = workersList_.get(
                lexManager_.input.toString());
        String startWorkerName = (String)tmpList.get(0);
        startWorkerName = startWorkerName.replaceAll("\\D+", "");
        if (startWorkerName.equals(""))
            return "Error: no worker for input directive ";

        int n = Integer.parseInt(startWorkerName);
        workerSeq_.get(n - 1).setInput(input_);

        for (int i = 0; i < workerSeq_.size(); i++) {
            ArrayList<String> consumersNames = workersList_.get(
                    lexManager_.worker.toString() + (i + 1));
            if(consumersNames.get(0).equals(lexManager_.output.toString()))
            {
                workerSeq_.get(i).setOutput(output_);
                continue;
            }

            for(int j = 0; j < consumersNames.size(); j++)
            {
                String tempSt = consumersNames.get(j).replaceAll(
                        "\\D+", "");
                n = Integer.parseInt(tempSt);
                int er = workerSeq_.get(i).setConsumer(workerSeq_.get(n - 1));
                if(er != 0)
                    return "Error: smthg wrong with consumers"
                            + j + " for worker" + (i + 1);
            }
        }
        return "";
    }

    /**
     * Set config for all workers
     * */
    private String SetConfigs_()
    {
        Iterator<Executor> elem = workerSeq_.iterator();
        int i = 1;
        while(elem.hasNext())
        {
            Executor exec = elem.next();
            exec.setConfig(workersConfigs_.get(
                    lexManager_.worker.toString() + i));
            i++;
        }
        return "";
    }

}